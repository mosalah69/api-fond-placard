DROP DATABASE IF EXISTS fondPlacard;
CREATE DATABASE fondPlacard;
USE fondPlacard;

-- Table ingredient
CREATE TABLE ingredient( 
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(64) NOT NULL

);

-- Table recipe
CREATE TABLE recipe(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(64) NOT NULL,
    category VARCHAR(64) NOT NULL,
    picture VARCHAR(64),
    score INT
 

);


create table recipe_ingredient
(
  ingredient_id int,
  recipe_id int,

  CONSTRAINT ingredient_cat_pk PRIMARY KEY (ingredient_id, recipe_id),

  CONSTRAINT FK_ingredient FOREIGN KEY (ingredient_id) REFERENCES ingredient (id),

  CONSTRAINT FK_recipe FOREIGN KEY (recipe_id) REFERENCES recipe (id)
);
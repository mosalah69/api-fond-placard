<?php
include('../../requeteHttp.php');

if( !empty($_GET['name']) ){

	$requete = $pdo->prepare("SELECT * FROM `recipe` WHERE `name` LIKE :nom");
	$requete->bindParam(':nom', $_GET['name']);
} else {

	$requete = $pdo->prepare("SELECT * FROM `recipe`");
}


if( $requete->execute() ){
	$resultats = $requete->fetchAll();

	
	$success = true;
	$data['nombre de resultat'] = count($resultats);
	$data['recipe'] = $resultats;
} else {
	$msg = "Une erreur s'est produite";
}

reponse_json($success, $data);
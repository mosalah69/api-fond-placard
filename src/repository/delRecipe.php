<?php
include('../../requeteHttp.php');

if( !empty($_GET['id']) ){


	$requete = $pdo->prepare("DELETE FROM `recipe` WHERE `id` = :id");
	$requete->bindParam(':id', $_GET['id']);

	if( $requete->execute() ){
		$success = true;
		$msg = 'La recette est supprimé';
	} else {
		$msg = "Une erreur s'est produite";
	}
} else {
	$msg = "Il manque des informations";
}

reponse_json($success, $data, $msg);
<?php

namespace App\Entity;

class Ingredient {
    private $id;
    private $name;

    

    public function __construct(string $name,  int $id = null) {
        $this->id = $id;
        $this->name = $name;
 
    }

    public function getName():string {
        return $this->name;
    }


    public function getId():int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
}
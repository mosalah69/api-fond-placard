<?php

namespace App\Entity;

class Recipe {
    private $id;
    private $name;
    private $category;
    private $picture;
    private $score;
    private $idIngredient;
    

    public function __construct(string $name, string $category, string $picture, int $id = null, int $score, int $idIngredient) {
        $this->id = $id;
        $this->name = $name;
        $this->category= $category;
        $this->picture = $picture;
        $this->score = $score;
        $this->idIngredient = $idIngredient;
    }

    public function getName():string {
        return $this->name;
    }
  
    public function getCategory():string {
        return $this->category;
    }

    public function getPicture():string {
        return $this->picture;
    }

    public function getScore():string {
        return $this->score;
    }

    public function getId():int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
}